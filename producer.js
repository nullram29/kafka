'use strict'

const BPromise = require('bluebird')

let KafkaConn = require('./conn')

class KafkaProducer extends KafkaConn {
  constructor (config) {
    super(config, 'Producer')

    this.logger = config.logger
    this.connOpts = Object.assign({
      'enable.idempotence': true,
      'dr_cb': true // delivery report callback
    }, this._connOpts)

    this.initialize(this.connOpts)
  }

  setEvent () {
    // scoping problem for this.logger inside delivery-report event
    // that is why we're using extra var
    let logger = this.logger

    this.handler.on('delivery-report', function (err, report) {
      if (err) logger.error(`Kafka producer err (potential data lost): ${JSON.stringify(err, null, 2)}`)
      logger.debug(`Kafka producer delivery-report: ${JSON.stringify(report, null, 2)}`)
    })

    setInterval(() => {
      this.handler.poll()
    }, 100)
  }

  toProduce (topic, data) {
    return new BPromise((resolve, reject) => {
      let isSent = this.handler.produce(
        topic,
        -1,
        Buffer.from(JSON.stringify(data)),
        null,
        Date.now()
      )

      if (isSent) return resolve()
      else {
        let errMsg = 'Kafka Producer err: not sent. '
        this.logger.error(errMsg)
        return reject(new Error(errMsg))
      }
    })
  }
}

module.exports = KafkaProducer
