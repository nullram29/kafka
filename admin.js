'use strict'

const BPromise = require('bluebird')
const Kafka = require('node-rdkafka')

let KafkaConf = require('./conf')

class KafkaAdmin extends KafkaConf {
  constructor (config) {
    super(config)

    this.logger = config.logger
    this.connOpts = Object.assign({
      'client.id': 'kafka-admin-' + config.pluginId
    }, this._connOpts)
    this.numPartitions = !(config.numPartitions) ? 10 : config.numPartitions
    this.replicationFactor = !(config.replicationFactor) ? 3 : config.replicationFactor

    this.adminConn = Kafka.AdminClient.create(this.connOpts)
  }

  connect () {
    return new BPromise((resolve) => {
      // This is synchronous
      this.logger.info('KafkaAdmin connected.')
      this.adminConn.connect()
      resolve(this)
    })
  }

  createTopic (topic) {
    // this is blocking, because connect and disconnect are sync
    return new BPromise((resolve, reject) => {
      return this.adminConn.createTopic(
        {
          topic,
          num_partitions: this.numPartitions,
          replication_factor: this.replicationFactor
        },
        (err) => {
          let errMsg = !err ? '' : err.message
          if (errMsg !== '' && errMsg.search('already exists.') === -1) {
            this.logger.error(`KafkaAdmin createTopic error: ${JSON.stringify(err, null, 2)}`)
            return reject(err)
          } else {
            return resolve(true)
          }
        }
      )
    })
  }

  disconnect () {
    return new BPromise((resolve) => {
      // This is synchronous
      this.logger.info('KafkaAdmin disconnected.')
      this.adminConn.disconnect()
      resolve(true)
    })
  }
}

module.exports = KafkaAdmin
