'use strict'

const BPromise = require('bluebird')

const KafkaAdmin = require('./admin')
const KafkaConsumer = require('./consumer')
const KafkaProducer = require('./producer')
const Topic = require('./topic')

class Kafka {
  constructor (conf) {
    this.admin = new KafkaAdmin(conf)
    this.producer = new KafkaProducer(conf)
    this.conf = conf

    this.topics = {}
  }

  createTopic (topicName) {
    return this.admin.createTopic(topicName).then(() => {
      /*
      Since Class Kafka is based on the structure of Class AMQP to prevent radical
      changes in the app/queue/*-queue.js level, the Class AMQP provides both
      consume and publish for all topic created, and upon checking the Platform
      Engine queues, it has two processes where it creates topic based on incoming
      msg content and publish some msg to them. But we know that creating multiple
      Kafka Consumer is one script, makes Kafka unstable and these topics that are
      created in the incoming msg content are in UUID format, we add a blocker to
      prevent the Class Kafka to create new Kafka Consumer.
      */
      if (topicName.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i) === null) {
        let conf = Object.assign({}, this.conf)
        conf.pluginId = topicName
        let consumer = null
        this.conf.logger.debug(`${topicName} to connect.`)
        consumer = new KafkaConsumer(conf)
        return consumer.connect().then(() => {
          return consumer
        })
      } else return null
    }).then((consumer) => {
      this.conf.logger.debug(`${topicName} to topic.`)
      let topic = new Topic(topicName, this.conf, this.producer, consumer)
      this.topics[topicName] = topic
      return BPromise.resolve()
    })
  }

  connect () {
    return BPromise.all([
      this.admin.connect(),
      this.producer.connect()
    ]).then(() => {
      return BPromise.resolve(this)
    })
  }

  disconnect () {
    return BPromise.all([
      this.admin.disconnect(),
      this.producer.disconnect(),
      BPromise.each(Object.keys(this.topics), topic => {
        if (topic.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i) === null) {
          return BPromise.resolve(this.topics[topic].kafkaConsumer.disconnect())
        } else return null
      })
    ]).then(() => {
      return BPromise.resolve(this)
    })
  }

  doGracefulShutdown () {
    return BPromise.each(Object.keys(this.topics), topic => {
      if (topic.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i) === null) {
        return BPromise.resolve(this.topics[topic].kafkaConsumer.doCommit())
          .then(() => this.topics[topic].kafkaConsumer.doPause())
          .then(() => this.topics[topic].kafkaConsumer.drainSpillOverQueue())
          .then(() => this.topics[topic].kafkaConsumer.doOverwrite())
      } else return null
    }).then(() => {
      setTimeout(() => {
        this.conf.logger.debug('done doGracefulShutdown')
        return BPromise.resolve(this)
      }, 1000)
    })
  }
}

module.exports = Kafka
