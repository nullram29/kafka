'use strict'

class Topic {
  constructor (topicName, config, kafkaProducer, kafkaConsumer = null) {
    this.topicName = topicName
    this.kafkaProducer = kafkaProducer
    this.kafkaConsumer = kafkaConsumer
    this.logger = config.logger

    this.kafkaProducer.setEvent()
  }

  publish (data) {
    return this.kafkaProducer.toProduce(this.topicName, data)
  }

  consume (handler) {
    if (this.kafkaConsumer !== null) return this.kafkaConsumer.toConsume(this.topicName, handler)
    else {
      this.logger.warn(`Kafka Topic ${this.topicName} not designed to consume.`)
      return true
    }
  }

  update () {
    this.kafkaConsumer.updateOffset()
    return true
  }
}

module.exports = Topic
