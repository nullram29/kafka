'use strict'

class KafkaConfig {
  constructor (config) {
    this._connOpts = {
      'metadata.broker.list': !(config.url) ? '' : config.url,
      'security.protocol': 'SASL_PLAINTEXT',
      'sasl.mechanisms': 'PLAIN',
      'sasl.username': !(config.user) ? '' : config.user,
      'sasl.password': !(config.pass) ? '' : config.pass,
      'socket.keepalive.enable': true
    }
    if (process.env.LOG_LEVEL === 'debug') {
      Object.assign(this._connOpts, {
        'debug': 'topic, queue, msg, cgrp, admin'
      })
    }
  }
}

module.exports = KafkaConfig
