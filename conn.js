'use strict'

const BPromise = require('bluebird')
const Kafka = require('node-rdkafka')

let KafkaConf = require('./conf')

class KafkaConnection extends KafkaConf {
  constructor (config, type) {
    super(config)

    this.logger = config.logger
    this.connOpts = null

    this.type = type
    this.handler = null
    this.isSIGTriggered = false
  }

  initialize (connOpts, topicOpts = null) {
    this.connOpts = connOpts
    if (topicOpts) this.handler = new Kafka[this.type](connOpts, topicOpts)
    else this.handler = new Kafka[this.type](connOpts)
    // NOTE: increase the value if you add new consumer* or publish*
    //   else you will encounter the 'possible memory leak error'
    this.handler.setMaxListeners(0)
  }

  connect () {
    this.logger.info(`Kafka connecting...`)

    return new BPromise((resolve) => {
      // logging debug messages, if debug is enabled
      this.handler.on('event.log', (log) => {
        this.logger.debug(`${this.handler.name} ?? Event: ${JSON.stringify(log, null, 2)}`)
      })

      this.handler.on('event.error', (err) => {
        this.logger.error(`Error :: ${err.code} | ${JSON.stringify(err, null, 2)}`)
        if (err.code === Kafka.CODES.ERRORS.ERR_UNKNOWN) process.emit('goAbort', err)
        else this.signalError()
      })

      this.handler.on('ready', () => {
        this.logger.info(`${this.connOpts['group.id']} ${this.handler.name} is ready.`)
        resolve(this.handler)
      })

      this.handler.on('disconnected', (arg) => {
        this.logger.info(`${this.handler.name} disconnected. ${JSON.stringify(arg, null, 2)}`)
      })

      this.handler.connect()
    })
  }

  disconnect () {
    return new BPromise((resolve) => {
      this.handler.disconnect()
      this.logger.info(`${this.handler.name} disconnected.`)
      resolve(this)
    })
  }

  signalError () {
    if (!this.isSIGTriggered) {
      process.emit('SIGTERM')
      this.isSIGTriggered = true
    }
    return true
  }
}

module.exports = KafkaConnection
