'use strict'

const BPromise = require('bluebird')
const Kafka = require('node-rdkafka')
const isEmpty = require('lodash').isEmpty

let KafkaConn = require('./conn')

class KafkaConsumer extends KafkaConn {
  constructor (config) {
    super(config, 'KafkaConsumer')

    this.log = config.logger

    this.topicName = ''
    this.partition = !(config.numPartitions) ? 10 : config.numPartitions

    // topic partition
    this.toppar = []
    // localOffset or paroff: index is partition,value is offset
    this.localOffset = new Array(this.partition)
    this.lastCommitted = []

    this.nextProcess = null

    this.processLength = 650
    this.intervalMs = 500
    this.spillOverQueue = []
    this.crntProcessLength = 0
    this.isPaused = false
    this.isSpillOver = false
    this.isRunning = false
    this.isFirst = true
    this.isOverwrite = false
    this.isOnRebalance = false

    this.connOpts = Object.assign({
      'group.id': config.pluginId,
      'fetch.message.max.bytes': 70 * 1024,
      'queued.max.messages.kbytes': 70,
      'enable.auto.offset.store': false,
      'enable.auto.commit': false,
      'rebalance_cb': (err, assignments) => {
        this.log.debug(`Kafka Rebalance: ${JSON.stringify(err, null, 2)}`)
        if (err.code === Kafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {
          // Note: this can throw when you are disconnected. Take care and wrap it in
          // a try catch if that matters to you
          // Code is -175
          this.handler.assign(assignments)
          this.updateLocalOffset(assignments)
          this.isOnRebalance = false
          this.toppar = this.handler.assignments()
          this.drainSpillOverQueue()
          this.doResume()
        } else if (err.code === Kafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS) {
          // Code is -174
          this.handler.unassign()
          this.isOnRebalance = true
          this.doPause()
          this.doCommit()
          this.drainSpillOverQueue()
        } else {
          // We had a real error
          this.log.error(`${Date.now()} Kafka Consumer Rebalance Error: `, err)
          process.emit('SIGTERM')
        }
      }
    }, this._connOpts)

    this.topicOpts = {
      'auto.offset.reset': 'smallest' // consume from the start
    }

    this.initialize(this.connOpts, this.topicOpts)
  }

  updateLocalOffset (newAssignments) {
    let newToppar = newAssignments.map(({ partition }) => partition)
    let newLocalOffset = new Array(this.partition)
    newToppar.forEach((value) => {
      newLocalOffset[value] = this.localOffset[value]
    })
    this.localOffset = newLocalOffset
  }

  toConsume (topic, nextPrcs) {
    return new BPromise((resolve) => {
      this.topicName = topic
      this.nextProcess = nextPrcs

      if (this.handler.isConnected()) {
        this.handler.subscribe([this.topicName])
        // start consuming messages
        this.handler.consume()
        // get topicPartition
        this.toppar = this.handler.assignments()
      }

      this.handler.on('data', (message) => {
        if (!this.isOnRebalance) {
          if (!this.toppar.length) this.toppar = this.handler.assignments()
          if (!isEmpty(message)) resolve(this.checkMessage(message))
          else resolve(true)
        } else resolve(true)
      })
    })
  }

  isDuplicate (message) {
    let msgPartition = Number(message.partition)
    let msgOffset = Number(message.offset)
    let localPartitionOffsetData = this.localOffset[msgPartition]
    let isDup = true

    if (localPartitionOffsetData === undefined || localPartitionOffsetData < msgOffset) {
      isDup = false
    }

    return isDup
  }

  checkMessage (message) {
    if (!this.isDuplicate(message)) {
      if (!this.isPaused) {
        if (this.crntProcessLength < this.processLength) {
          return BPromise.resolve(this.toNextProcess(message))
        } else {
          this.doPause()
          this.spillOverQueue.push(message)
          this.runProcessInterval()
          return BPromise.resolve(true)
        }
      } else {
        this.spillOverQueue.push(message)
        return BPromise.resolve(true)
      }
    } else return BPromise.resolve(true)
  }

  toNextProcess (message) {
    this.log.debug(`toNextProcess.`)
    // copy value of value field to content field
    // so there would be less changes in the processing of msg
    let value = message.value.toString()
    delete message.value
    message.content = value

    this.crntProcessLength += 1
    // update offset, we consider this as processed msg
    if ((this.localOffset[Number(message.partition)] === undefined) || this.localOffset[Number(message.partition)] < Number(message.offset)) {
      this.localOffset[Number(message.partition)] = Number(message.offset)
    } else this.log.debug(`anomaly... p ${message.partition} : o ${this.localOffset[Number(message.partition)]} vs o ${message.offset}`)
    this.log.debug(`  p ${message.partition}: o ${message.offset}`)

    return this.nextProcess(message)
  }

  /*
  updateOffset function
    on first run, signals to run the commitInterval - which runs eternally -,
    is responsible counting the current msgs being processed.
  */
  updateOffset () {
    this.log.debug('update offset...')
    if (this.isFirst) {
      this.isFirst = false
      this.runCommitInterval()
    }

    this.crntProcessLength -= 1

    return true
  }

  createTopparoff () {
    let data = []
    let forCommit = [...this.localOffset]

    forCommit.forEach((element, index) => {
      if (element >= 0) {
        data.push({
          topic: this.topicName,
          partition: index,
          offset: element + 1
        })
      }
    })

    this.lastCommitted = [...forCommit]

    return data
  }

  doCommit () {
    return BPromise.resolve(this.createTopparoff())
      .then((commitData) => {
        if (commitData.length) {
          this.log.debug(`commitData: `, JSON.stringify(commitData, null, 2))
          this.handler.commit(commitData)
          this.log.debug('committed...')
        }
        return true
      })
  }

  // This is a clock work
  runCommitInterval () {
    setInterval(() => {
      let notEqualOffsets = !this._equals(this.localOffset, this.lastCommitted)

      if (!this.lastCommitted.length || notEqualOffsets) {
        this.log.debug('to commit...')
        this.doCommit()
      }
    }, this.intervalMs)
  }

  // This is a clock work
  runProcessInterval () {
    if (!this.isRunning) {
      this.isRunning = true
      setInterval(() => {
        if (!this.isSpillOver && this.crntProcessLength < this.processLength) {
          if (this.spillOverQueue.length) {
            this.processSpillOverQueue()
          } else {
            // we can now receive msgs from client again
            this.doResume()
          }
        }
      }, this.intervalMs)
    }
  }

  processSpillOverQueue () {
    if (!this.isSpillOver) {
      this.isSpillOver = true
      this.log.debug(`processSpillOverQueue this.spillOverQueue.length: `, this.spillOverQueue.length)
      // this.spillOverQueue = this.spillOverQueue.reverse()
      BPromise.map(this.spillOverQueue, (message) => {
        this.log.debug(`message..`)
        return this.toNextProcess(message)
      }, { concurrency: 2 }).then(() => {
        this.spillOverQueue = []
        this.isSpillOver = false
        this.log.debug(`done.`)
      })
    }
  }

  doPause () {
    if (!this.isOverwrite) {
      if (!this.isPaused) this.handler.pause(this.toppar)
      this.isPaused = true
    }
    return true
  }

  doResume () {
    if (!this.isOverwrite) {
      if (this.isPaused) this.handler.resume(this.toppar)
      this.isPaused = false
    }
    return true
  }

  // This is only called during shutdown
  doOverwrite () {
    this.isOverwrite = true
    return true
  }

  drainSpillOverQueue () {
    this.spillOverQueue = []
    return true
  }

  /*
  _equals function
  This is only used for checking if the localOffset - which is the current offset;
  which is arr1 - and the lastCommitted - which holds the last committed offset;
  which is arr2 - are same.
  */
  _equals (arr1, arr2) {
    return arr1.length === arr2.length && arr1.every((u, i) => u === arr2[i])
  }
}

module.exports = KafkaConsumer
